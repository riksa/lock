/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.ui;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;

import com.github.machinarius.preferencefragment.PreferenceFragment;
import com.squareup.leakcanary.RefWatcher;

import org.riksa.android.screenlockprovider.LockProviderApplication;
import org.riksa.android.screenlockprovider.R;
import org.riksa.android.screenlockprovider.dagger.ApplicationComponent;
import org.riksa.android.screenlockprovider.service.LockService;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment {
    private static final String PREFERENCE_VERSION = "preference_version";
    private static final String PREFERENCE_LAUNCHER = "preference_launcher";
    private static final String PREFERENCE_LOCK = "preference_lock";
    private static final String PREFERENCE_ENABLED = "preference_enabled";
    private static final String PREFERENCE_TUTORIAL_SHOWN = "preference_tutorial_shown";

    @Inject
    @Named("versionString")
    protected String versionString;

    @Inject
    @Named("launcherPackageNames")
    protected String[] launcherPackageNames;

    @Inject
    @Named("launcherLabels")
    protected String[] launcherLabels;

    @Inject
    @Named("lockPackageNames")
    protected String[] lockPackageNames;

    @Inject
    @Named("lockLabels")
    protected String[] lockLabels;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        getApplicationComponent().inject(this);

        addPreferencesFromResource(R.xml.preferences);

        final CheckBoxPreference preferenceEnabled = (CheckBoxPreference) findPreference(PREFERENCE_ENABLED);
        preferenceEnabled.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if ((Boolean) newValue) {
                    Timber.d("Starting service");
                    LockService.start(preference.getContext());
                } else {
                    Timber.d("Stopping service");
                    LockService.stop(preference.getContext());
                }
                return true;
            }
        });

        final ListPreference preferenceLauncher = (ListPreference) findPreference(PREFERENCE_LAUNCHER);
        preferenceLauncher.setEntries(launcherLabels);
        preferenceLauncher.setEntryValues(launcherPackageNames);

        final ListPreference preferenceLock = (ListPreference) findPreference(PREFERENCE_LOCK);
        preferenceLock.setEntries(lockLabels);
        preferenceLock.setEntryValues(lockPackageNames);

        final Preference preferenceVersion = findPreference(PREFERENCE_VERSION);
        preferenceVersion.setSummary(versionString);

    }

    @Override
    public void onDestroy() {
        final CheckBoxPreference preferenceEnabled = (CheckBoxPreference) findPreference(PREFERENCE_ENABLED);
        preferenceEnabled.setOnPreferenceChangeListener(null);

        RefWatcher refWatcher = LockProviderApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);

        super.onDestroy();
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((LockProviderApplication) getActivity().getApplicationContext()).getApplicationComponent();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    static AlertDialog showLauncherChooserDialog(final Context context, final String[] labels, final String[] packageNames, final DialogInterface.OnClickListener onPositiveButtonListener) {
        final SharedPreferences defaultSharedPreferences = getSharedPreferences(context);

        final DialogInterface.OnClickListener onSelectionChangedListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Timber.d("Clicked %d, %s (%s)", which, labels[which], packageNames[which]);
                final String packageName = packageNames[which];
                defaultSharedPreferences.edit()
                        .putString(SettingsFragment.PREFERENCE_LAUNCHER, packageName)
                        .commit();
            }
        };

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.preference_launcher_dialog_title);
        builder.setSingleChoiceItems(labels, 0, onSelectionChangedListener);
        builder.setPositiveButton(android.R.string.ok, onPositiveButtonListener);

        return builder.show();
    }

    public static AlertDialog showLockChooserDialog(final Context context, final String[] labels, final String[] packageNames, final DialogInterface.OnClickListener onPositiveButtonListener) {
        final SharedPreferences defaultSharedPreferences = getSharedPreferences(context);

        final DialogInterface.OnClickListener onSelectionChangedListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Timber.d("Clicked %d, %s (%s)", which, labels[which], packageNames[which]);
                final String packageName = packageNames[which];
                defaultSharedPreferences.edit()
                        .putString(SettingsFragment.PREFERENCE_LOCK, packageName)
                        .commit();
            }
        };

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.preference_lock_dialog_title);
        builder.setSingleChoiceItems(labels, 0, onSelectionChangedListener);
        builder.setPositiveButton(android.R.string.ok, onPositiveButtonListener);

        return builder.show();
    }


    public static String getSelectedLauncerPackage(Context context) {
        return getSharedPreferences(context).getString(SettingsFragment.PREFERENCE_LAUNCHER, null);
    }

    public static void setSelectedLauncerPackage(Context context, String packageName) {
        getSharedPreferences(context).edit().putString(SettingsFragment.PREFERENCE_LAUNCHER, packageName).commit();
    }


    public static String getSelectedLockPackage(Context context) {
        return getSharedPreferences(context).getString(SettingsFragment.PREFERENCE_LOCK, null);
    }

    public static void setSelectedLockPackage(Context context, String packageName, String activityName) {
        // TODO: support multiple skins within package
        getSharedPreferences(context).edit().putString(SettingsFragment.PREFERENCE_LOCK, packageName).commit();
    }

    public static boolean isEnabled(Context context) {
        return getSharedPreferences(context).getBoolean(SettingsFragment.PREFERENCE_ENABLED, true);
    }

    public static boolean isTutorialShown(Context context) {
        return getSharedPreferences(context).getBoolean(SettingsFragment.PREFERENCE_TUTORIAL_SHOWN, false);
    }

    public static void setTutorialShown(Context context, boolean shown) {
        getSharedPreferences(context)
                .edit()
                .putBoolean(PREFERENCE_TUTORIAL_SHOWN, shown)
                .commit();
    }

}
