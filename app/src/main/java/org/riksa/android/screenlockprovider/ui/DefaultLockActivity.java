/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import org.riksa.android.screenlockprovider.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DefaultLockActivity extends DaggerActivity {

    public static final String ORG_RIKSA_ANDROID_GLYPHLOCK = "org.riksa.android.glyphlock";
    @Bind(R.id.coordinator)

    protected CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_lock);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.button)
    public void onClickUnlock() {
        setResult(1);
        finish();
    }

    @OnClick(R.id.link)
    public void onClickLink() {
        Snackbar.make(coordinatorLayout, R.string.snack_link_title, Snackbar.LENGTH_SHORT)
                .setAction(R.string.snack_link_action, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openPlayStore(ORG_RIKSA_ANDROID_GLYPHLOCK);
                    }
                })
                .show();
    }

    private void openPlayStore(String packageName) {

        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

}
