/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.dagger;

import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;

import org.riksa.android.screenlockprovider.BuildConfig;

import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by riksa on 09/08/15.
 */
@Module
public class AndroidModule {
    private static final String CATEGORY_LOCK = "org.riksa.android.screenlockprovider.LOCK";
    private final Application application;
    /**
     * Do not use package name from packagemanager, because we need to filter out both release and
     * debug versions "org.riksa.android.screenlockprovider" and "org.riksa.android.screenlockprovider.debug"
     */
    private static final String LOCK_PACKAGE = "org.riksa.android.screenlockprovider";

    public AndroidModule(Application application) {
        this.application = application;
    }

    @Provides
    @Named("versionCode")
    int provideVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    @Provides
    @Named("versionName")
    String provideVersionName() {
        return BuildConfig.VERSION_NAME;
    }

    @Provides
    @Named("versionString")
    String provideVersionString(@Named("versionCode") int versionCode, @Named("versionName") String versionName) {
        return String.format("%s (%d)", versionName, versionCode);
    }

    @Provides
    @Named("launcherResolveInfo")
    List<ResolveInfo> provideLaunchers() {
        final PackageManager packageManager = application.getPackageManager();

        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_HOME);

        final String packageName = application.getPackageName();

        return FluentIterable.from(packageManager.queryIntentActivities(mainIntent, 0))
                .filter(new Predicate<ResolveInfo>() {
                    @Override
                    public boolean apply(ResolveInfo input) {
                        // filter out our own package
                        return !input.activityInfo.applicationInfo.packageName.startsWith(LOCK_PACKAGE);
                    }
                }).toList();
    }

    @Provides
    @Named("launcherLabels")
    String[] provideLauncherLabels(@Named("launcherResolveInfo") List<ResolveInfo> pkgAppsList) {
        final PackageManager packageManager = application.getPackageManager();

        return FluentIterable.from(pkgAppsList)
                .transform(new Function<ResolveInfo, String>() {
                    @Override
                    public String apply(ResolveInfo input) {
                        return String.valueOf(input.loadLabel(packageManager));
                    }
                })
                .toArray(String.class);
    }

    @Provides
    @Named("launcherPackageNames")
    String[] provideLauncherPackageNames(@Named("launcherResolveInfo") List<ResolveInfo> pkgAppsList) {
        return FluentIterable.from(pkgAppsList)
                .transform(new Function<ResolveInfo, String>() {
                    @Override
                    public String apply(ResolveInfo input) {
                        return input.activityInfo.applicationInfo.packageName;
                    }
                })
                .toArray(String.class);
    }

    @Provides
    @Named("lockResolveInfo")
    List<ResolveInfo> provideLocks() {
        final PackageManager packageManager = application.getPackageManager();

        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(CATEGORY_LOCK);

        final String packageName = application.getPackageName();

        return FluentIterable.from(packageManager.queryIntentActivities(mainIntent, 0))
                .filter(new Predicate<ResolveInfo>() {
                    @Override
                    public boolean apply(ResolveInfo input) {
                        // activity needs to be exported for us to launch
                        return input.activityInfo.exported;
                    }
                })
                .toList();
    }

    @Provides
    @Named("lockLabels")
    String[] provideLockLabels(@Named("lockResolveInfo") List<ResolveInfo> pkgAppsList) {
        final PackageManager packageManager = application.getPackageManager();

        return FluentIterable.from(pkgAppsList)
                .transform(new Function<ResolveInfo, String>() {
                    @Override
                    public String apply(ResolveInfo input) {
                        return String.valueOf(input.loadLabel(packageManager));
                    }
                })
                .toArray(String.class);
    }

    @Provides
    @Named("lockPackageNames")
    String[] provideLockPackageNames(@Named("lockResolveInfo") List<ResolveInfo> pkgAppsList) {
        return FluentIterable.from(pkgAppsList)
                .transform(new Function<ResolveInfo, String>() {
                    @Override
                    public String apply(ResolveInfo input) {
                        return input.activityInfo.applicationInfo.packageName;
                    }
                })
                .toArray(String.class);
    }

}
