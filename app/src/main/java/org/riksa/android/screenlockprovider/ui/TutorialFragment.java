/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.ui;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.riksa.android.screenlockprovider.R;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.Lazy;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TutorialFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TutorialFragment extends DaggerFragment {
    private static final String ARG_PARAM_LAYOUT = "layout";
    public static final String SKIN = "org.riksa.android.glyphlock";

    private int layout = R.layout.fragment_tutorial_1;
    private OnFragmentInteractionListener mListener;
    private static final int REQUEST_CODE_SETTINGS = 1;
    private static final int REQUEST_CODE_SKIN = 2;

    @Nullable
    @Bind(R.id.spinner_launcher)
    protected Spinner spinnerLauncher;

    @Nullable
    @Bind(R.id.spinner_skin)
    protected Spinner spinnerSkin;

    @Inject
    @Named("launcherPackageNames")
    protected Lazy<String[]> launcherPackageNames;

    @Inject
    @Named("launcherLabels")
    protected Lazy<String[]> launcherLabels;

    @Inject
    @Named("lockPackageNames")
    protected Lazy<String[]> lockPackageNames;

    @Inject
    @Named("lockLabels")
    protected Lazy<String[]> lockLabels;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param layout resource id of the layout
     * @return A new instance of fragment TutorialFragment.
     */
    public static TutorialFragment newInstance(int layout) {
        TutorialFragment fragment = new TutorialFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM_LAYOUT, layout);
        fragment.setArguments(args);
        return fragment;
    }

    public TutorialFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationComponent().inject(this);
        if (getArguments() != null) {
            layout = getArguments().getInt(ARG_PARAM_LAYOUT, layout);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        if (spinnerLauncher != null) {
            // setup launcher spinner
            ArrayAdapter adapterLauncher = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, Arrays.asList(launcherLabels.get()));
            spinnerLauncher.setAdapter(adapterLauncher);

            final List<String> launcherPackageNameList = Arrays.asList(launcherPackageNames.get());
            final int launcherIndex = launcherPackageNameList.indexOf(SettingsFragment.getSelectedLauncerPackage(getActivity()));
            spinnerLauncher.setSelection(launcherIndex != -1 ? launcherIndex : 0);
            if (launcherIndex == -1) {
                selectLauncher(0);
            }
            spinnerLauncher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectLauncher(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            // setup skin spinner
            ArrayAdapter adapterSkin = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, Arrays.asList(lockLabels.get()));
            spinnerSkin.setAdapter(adapterSkin);

            final List<String> lockPackageNameList = Arrays.asList(lockPackageNames.get());
            final int lockIndex = lockPackageNameList.indexOf(SettingsFragment.getSelectedLockPackage(getActivity()));
            spinnerSkin.setSelection(lockIndex != -1 ? lockIndex : 0);
            if (lockIndex == -1) {
                selectSkin(0);
            }
            spinnerSkin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectSkin(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    private void selectLauncher(int i) {
        String packageName = launcherPackageNames.get()[i];
        Timber.d("Chose launcher %s", packageName);
        SettingsFragment.setSelectedLauncerPackage(getActivity(), packageName);
    }

    private void selectSkin(int i) {
        String packageName = lockPackageNames.get()[i];
        Timber.d("Chose skin %s", packageName);
        SettingsFragment.setSelectedLockPackage(getActivity(), packageName, null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Nullable
    @OnClick({R.id.fab_home_settings, R.id.fab_keyguard_settings})
    public void onClickHomeSettings() {
        Timber.d("Show home settings");
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        startActivityForResult(intent, REQUEST_CODE_SETTINGS);
    }

    @Nullable
    @OnClick(R.id.fab_skin)
    public void onClickSkin() {
        try {
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + SKIN)), REQUEST_CODE_SKIN);
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + SKIN)), REQUEST_CODE_SKIN);
        }
    }

    @Nullable
    @OnClick(R.id.fab_done)
    public void onClickDone() {
        mListener.onDone();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            default:
            case REQUEST_CODE_SETTINGS:
            case REQUEST_CODE_SKIN:
                mListener.onNextPage();
                break;
        }
    }

    public interface OnFragmentInteractionListener {
        void onNextPage();

        void onDone();
    }

}
