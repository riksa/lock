/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import timber.log.Timber;

public class LockService extends Service {
    private ScreenOffReceiver receiver;

    public LockService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        Timber.e(new Throwable(), "onBind not allowed, just start the service %s", intent);
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("onCreate");

        registerReceiver();
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);

        unregisterReceiver();
        receiver = new ScreenOffReceiver();
        registerReceiver(receiver, intentFilter);
    }

    private void unregisterReceiver() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
    }

    @Override
    public void onDestroy() {
        Timber.d("onDestroy");
        unregisterReceiver();
        super.onDestroy();

    }

    public static ComponentName start(Context context) {
        Timber.d("start service");
        Intent service = new Intent(context, LockService.class);
        return context.startService(service);
    }

    public static boolean stop(Context context) {
        Timber.d("stop service");
        Intent service = new Intent(context, LockService.class);
        return context.stopService(service);
    }

}
