/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.ui;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import org.riksa.android.screenlockprovider.R;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TutorialActivity extends DaggerActivity implements TutorialFragment.OnFragmentInteractionListener {

    public static final String EXTRA_TAB = "extra_tab";
    public static final int TAB_LAUNCHER = 3;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.pager)
    ViewPager mViewPager;

    @Bind(R.id.sliding_tabs)
    TabLayout tabLayout;

    SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        getApplicationComponent().inject(this);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setTabsFromPagerAdapter(mSectionsPagerAdapter);

        tabLayout.setupWithViewPager(mViewPager);

        if (getIntent().hasExtra(EXTRA_TAB)) {
            mViewPager.setCurrentItem(getIntent().getIntExtra(EXTRA_TAB, 0));
        }

    }

    @Override
    public void onNextPage() {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
    }

    @Override
    public void onDone() {
        finish();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return TutorialFragment.newInstance(R.layout.fragment_tutorial_4);
                case 1:
                    return TutorialFragment.newInstance(R.layout.fragment_tutorial_2);
                case 2:
                    return TutorialFragment.newInstance(R.layout.fragment_tutorial_3);
                case TAB_LAUNCHER:
                    return TutorialFragment.newInstance(R.layout.fragment_tutorial_5);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                default:
                case 0:
                    return getString(R.string.title_tutorial_1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_tutorial_2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_tutorial_3).toUpperCase(l);
                case 3:
                    return getString(R.string.title_tutorial_4).toUpperCase(l);
            }
        }
    }


}
