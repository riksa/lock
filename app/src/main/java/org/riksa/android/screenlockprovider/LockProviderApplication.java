/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider;

import android.app.Application;
import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.WindowManager;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import org.riksa.android.screenlockprovider.dagger.AndroidModule;
import org.riksa.android.screenlockprovider.dagger.ApplicationComponent;
import org.riksa.android.screenlockprovider.dagger.DaggerApplicationComponent;
import org.riksa.android.screenlockprovider.service.LockService;
import org.riksa.android.screenlockprovider.ui.SettingsFragment;
import org.riksa.android.screenlockprovider.ui.SystemViewGroup;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Lazy;
import timber.log.Timber;

/**
 * Created by riksa on 18/08/15.
 */
public class LockProviderApplication extends Application {
    private ApplicationComponent component;
    private boolean locked = false;
    private RefWatcher refWatcher;

    @Inject
    @Named("launcherPackageNames")
    protected Lazy<String[]> launcherPackageNames;

    @Inject
    @Named("lockPackageNames")
    protected Lazy<String[]> lockPackageNames;

    private SystemViewGroup systemViewGroup;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        refWatcher = LeakCanary.install(this);

        component = DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();

        component.inject(this);
        if (SettingsFragment.isEnabled(this)) {
            LockService.start(this);
        }

        // make sure we have some launcher and skin selected
        initialSetupIfNeeded();
    }

    private void initialSetupIfNeeded() {
        final List<String> launcherPackageNameList = Arrays.asList(launcherPackageNames.get());
        final int launcherIndex = launcherPackageNameList.indexOf(SettingsFragment.getSelectedLauncerPackage(this));
        if (launcherIndex == -1) {
            String packageName = launcherPackageNameList.get(0);
            Timber.d("Initial launcher %s", packageName);
            SettingsFragment.setSelectedLauncerPackage(this, packageName);
        }

        final List<String> lockPackageNameList = Arrays.asList(lockPackageNames.get());
        final int lockIndex = lockPackageNameList.indexOf(SettingsFragment.getSelectedLockPackage(this));
        if (lockIndex == -1) {
            String packageName = lockPackageNameList.get(0);
            Timber.d("Initial lock %s", packageName);
            SettingsFragment.setSelectedLockPackage(this, packageName, null); // TODO: support multiple locks within package
        }
    }

    public static RefWatcher getRefWatcher(Context context) {
        LockProviderApplication application = (LockProviderApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    public ApplicationComponent getApplicationComponent() {
        return component;
    }

    public void setLocked(boolean locked) {
        Timber.d("setLock %b", locked);
        this.locked = locked;
    }

    public boolean isLocked() {
        return locked;
    }

    public void addSystemViewGroup() {
        Timber.d("addSystemViewGroup");
        removeSystemViewGroup();

        WindowManager manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (50 * getResources()
                .getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        systemViewGroup = new SystemViewGroup(this);
        manager.addView(systemViewGroup, localLayoutParams);
    }

    public void removeSystemViewGroup() {
        Timber.d("removeSystemViewGroup");

        if( systemViewGroup != null ) {
            WindowManager manager = ((WindowManager) getApplicationContext()
                    .getSystemService(Context.WINDOW_SERVICE));
            manager.removeView(systemViewGroup);
            systemViewGroup = null;
        }
    }



}
