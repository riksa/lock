/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.riksa.android.screenlockprovider.LockProviderApplication;
import org.riksa.android.screenlockprovider.ui.LockActivity;
import org.riksa.android.screenlockprovider.ui.SettingsFragment;

import timber.log.Timber;

/**
 * {@link BroadcastReceiver} created by {@link LockService} when it is started to receive
 * {@link Intent.ACTION_SCREEN_OFF} intents to enable lock
 */
public class ScreenOffReceiver extends BroadcastReceiver {
    public ScreenOffReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean enabled = SettingsFragment.isEnabled(context);
        Timber.d("onReceive %s, Lock is %s", intent, enabled ? "enabled" : "disabled");

        final String action = intent.getAction();
        if (enabled && action != null) {
            switch (action) {
                case Intent.ACTION_SCREEN_OFF:
                    Timber.d("Screen turned off");
                    createLock(context.getApplicationContext());
                    break;
                case Intent.ACTION_SCREEN_ON:
                    // dunno if we need this one at all
                    Timber.d("Screen turned on");
                    break;
            }

        }
    }

    private void createLock(Context context) {
        Timber.d("createLock");
        LockProviderApplication application = (LockProviderApplication) context.getApplicationContext();
        application.setLocked(true);

        Intent intent = new Intent(context, LockActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
