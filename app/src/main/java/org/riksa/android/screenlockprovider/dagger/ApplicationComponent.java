/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.dagger;

import org.riksa.android.screenlockprovider.LockProviderApplication;
import org.riksa.android.screenlockprovider.ui.LockActivity;
import org.riksa.android.screenlockprovider.ui.MainActivity;
import org.riksa.android.screenlockprovider.ui.SettingsFragment;
import org.riksa.android.screenlockprovider.ui.TutorialActivity;
import org.riksa.android.screenlockprovider.ui.TutorialFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by riksa on 09/08/15.
 */
@Singleton
@Component(modules = AndroidModule.class)
public interface ApplicationComponent {
    void inject(MainActivity activity);

    void inject(LockActivity lockActivity);

    void inject(SettingsFragment activity);

    void inject(TutorialActivity tutorialActivity);

    void inject(TutorialFragment tutorialFragment);

    void inject(LockProviderApplication lockProviderApplication);
}
