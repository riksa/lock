/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import org.riksa.android.screenlockprovider.LockProviderApplication;
import org.riksa.android.screenlockprovider.R;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;

public class LockActivity extends DaggerActivity {
    public static final String CATEGORY_LOCK = "org.riksa.android.screenlockprovider.LOCK";

    private static final int REQUEST_CODE_UNLOCK = 1;

    @Inject
    @Named("launcherPackageNames")
    protected String[] launcherPackageNames;

    @Inject
    @Named("launcherLabels")
    protected String[] launcherLabels;

    @Inject
    @Named("lockPackageNames")
    protected String[] lockPackageNames;

    @Inject
    @Named("lockLabels")
    protected String[] lockLabels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        super.onCreate(savedInstanceState);

        Timber.d("onCreate");
        if (!getLockApplication().isLocked()) {
            Timber.d("unlocked, show launcher");
            unlockDevice();
            return;
        }
        getLockApplication().addSystemViewGroup();


        getApplicationComponent().inject(this);

        setContentView(R.layout.activity_lock);
        showLocker();
    }


    @Override
    protected void onDestroy() {
        getLockApplication().removeSystemViewGroup();

        super.onDestroy();
    }

    private void showLocker() {
        final String packageName = SettingsFragment.getSelectedLockPackage(this);
        if (packageName == null) {
            // Show launcher picker
            showLockerChooser();

        } else {
            startLocker(packageName);
        }
    }

    private void showLockerChooser() {
        final Context context = this;
        final DialogInterface.OnClickListener onPositiveButtonListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String packageName = SettingsFragment.getSelectedLockPackage(context);
                startLocker(packageName);
            }
        };

        SettingsFragment.showLockChooserDialog(this, lockLabels, lockPackageNames, onPositiveButtonListener);

    }

    private void startLocker(String packageName) {
        Timber.d("Start locker %s", packageName);

        final Intent launchIntentForPackage = getLockIntentForPackageAndActivity(getPackageManager(), packageName, null);
        Timber.d("LaunchIntent %s", launchIntentForPackage);
//        Intent intent = new Intent("org.riksa.android.glyphlock.debug#org.riksa.android.glyphlock.GlyphLockActivity");

        startActivityForResult(launchIntentForPackage, REQUEST_CODE_UNLOCK);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Timber.d("onActivityResult %d, %d, %s", requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_UNLOCK && resultCode != 0) {
            // unlocked
//            unlockDevice();
            getLockApplication().setLocked(false);
            finish();
        } else {
            showLocker();
        }
    }

    private void unlockDevice() {
        getLockApplication().setLocked(false);

        final String packageName = SettingsFragment.getSelectedLauncerPackage(this);
        if (packageName == null) {
            // Show launcher picker
            showLauncherChooser();

        } else {
            startLauncher(packageName);
        }
    }

    private LockProviderApplication getLockApplication() {
        return (LockProviderApplication) getApplication();
    }

    private void showLauncherChooser() {
        final Context context = this;
        final DialogInterface.OnClickListener onPositiveButtonListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String packageName = SettingsFragment.getSelectedLauncerPackage(context);
                startLauncher(packageName);
            }
        };

        SettingsFragment.showLauncherChooserDialog(this, launcherLabels, launcherPackageNames, onPositiveButtonListener);

    }

    private void startLauncher(String packageName) {
        getLockApplication().removeSystemViewGroup();
        Timber.d("Starting launcher %s", packageName);
        final Intent launchIntentForPackage = getLaunchIntentForPackage(getPackageManager(), packageName);

        if (launchIntentForPackage == null) {
            showLauncherChooser();
        } else {
            // start the launcher
            startActivity(launchIntentForPackage);
            // finish lock screen
            finish();
        }

    }

    public static Intent getLaunchIntentForPackage(PackageManager packageManager, String packageName) {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setPackage(packageName);
        ResolveInfo resolveInfo = packageManager.resolveActivity(homeIntent, 0);
        if (resolveInfo != null) {
            Intent intent = new Intent(homeIntent);
            intent.setClassName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name);
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            return intent;
        }
        return null;
    }

    public static Intent getLockIntentForPackageAndActivity(PackageManager packageManager, String packageName, String activityName) {
        // TODO: use activityname as well to support multiple locks within package
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(CATEGORY_LOCK);
        homeIntent.setPackage(packageName);
        ResolveInfo resolveInfo = packageManager.resolveActivity(homeIntent, 0);
        if (resolveInfo != null) {
            Intent intent = new Intent(homeIntent);
            intent.setClassName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name);
            intent.setAction(Intent.ACTION_MAIN);
            return intent;
        }
        return null;
    }

}
