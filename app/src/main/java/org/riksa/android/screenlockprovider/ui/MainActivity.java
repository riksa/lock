/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.screenlockprovider.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import org.riksa.android.screenlockprovider.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends DaggerActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationComponent().inject(this);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            final SettingsFragment fragment = new SettingsFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, fragment)
                    .commit();
        }

        if (!SettingsFragment.isTutorialShown(this)) {
            startActivity(new Intent(this, TutorialActivity.class));
            SettingsFragment.setTutorialShown(this, true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_help:
                startActivity(new Intent(this, TutorialActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (setupNeeded()) {
            Timber.d("Setup needed");
            Intent intent = new Intent(this, TutorialActivity.class);
            intent.putExtra(TutorialActivity.EXTRA_TAB, TutorialActivity.TAB_LAUNCHER);
            startActivity(intent);
        }
    }

    private boolean setupNeeded() {
        final String selectedLauncerPackage = SettingsFragment.getSelectedLauncerPackage(this);

        if (TextUtils.isEmpty(selectedLauncerPackage)) {
            return true;
        }

        final String selectedLockPackage = SettingsFragment.getSelectedLockPackage(this);

        if (TextUtils.isEmpty(selectedLockPackage)) {
            return true;
        }

        Intent launchIntentForPackage = LockActivity.getLaunchIntentForPackage(getPackageManager(), selectedLauncerPackage);
        if (launchIntentForPackage == null) {
            // launcher removed or not selected
            return true;
        }

        Intent lockIntentForPackageAndActivity = LockActivity.getLockIntentForPackageAndActivity(getPackageManager(), selectedLockPackage, null);// TODO: support multiple activities within package
        if (lockIntentForPackageAndActivity == null) {
            return true;
        }

        return false;
    }

}
