# Lock! #

Lock screen application that supports "Lock skins".

# Known issues

The application will not prevent anyone from accessing your device. The status bar can be pulled down when the lock is shown, allowing anyone to bypass the lock. This is merely an entertainment application, not an alternative way to protect your device! I do have an idea how to prevent the status bar from being opened, and I might try it at some point, but for now the lock is not really secure replacement for real screen lock :)

# Creating skins #
If you want to create a lock skin, you will only need to create an activity that the **Lock!** can find and launch. Once launched, the activity should return with a non-zero return code to unlock the device. Check the *DefaultLockActivity* for an example implementation.

## Make you activity known and accessible to "Lock!"

 1. Your activity needs to declare an intent-filter with category *org.riksa.android.screenlockprovider.LOCK* and action *android.intent.action.MAIN*
 2. Your activity needs to be exported *android:exported="true"*

        <activity
            android:name=".ui.DefaultLockActivity"
            android:exported="true"
            android:label="@string/title_activity_default_lock"
            android:theme="@style/FullscreenTheme">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="org.riksa.android.screenlockprovider.LOCK" />
                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </activity>

Now you can install the application to your device and from the *Lock!* settings you should be able to select your skin.

### Who do I talk to? ###

* riku salkia <riksa@iki.fi>
